package br.uniriotec.bsi.servicociclista;



import java.util.ArrayList;



public class BicicletaMock {
	private String id;
	private int number;
	private BicicletaStatus status;
	public static ArrayList<BicicletaMock> bicicletas;
	
	public BicicletaMock(String id, int number, BicicletaStatus status) {
		this.id = id;
		this.number = number;
		this.status = status;
	}
	
	public static BicicletaMock getBicicletaById(String id) {
		for(int i=0; i < bicicletas.size(); i++) {
			if (bicicletas.get(i).id == id){
				return bicicletas.get(i);
			}
		}
		return null;
	}
	
	public static void insertBicicleta(BicicletaMock bicicleta) {
		bicicletas.add(bicicleta);
	}
	
	public static void criaInstancias(){
		BicicletaMock bicicleta = 
				new BicicletaMock("01", 13232, BicicletaStatus.DISPONIVEL);
		BicicletaMock bicicleta2 = 
				new BicicletaMock("02", 83282, BicicletaStatus.DISPONIVEL);
		BicicletaMock bicicleta3 = 
				new BicicletaMock("03", 92774, BicicletaStatus.EMUSO);
		
		BicicletaMock.insertBicicleta(bicicleta);
		BicicletaMock.insertBicicleta(bicicleta2);
		BicicletaMock.insertBicicleta(bicicleta3);
	}
	
	public BicicletaStatus getStatus(){
		return this.status;
	}
	
	public void setStatus(BicicletaStatus status){
		this.status = status;
	}
	
	
	public enum BicicletaStatus{
		NOVA,
		EMUSO,
		DISPONIVEL,
		REPAROSOLICITADO,
		EMREPARO,
		APOSENTADA
	}
	
}
