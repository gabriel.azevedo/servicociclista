package br.uniriotec.bsi.servicociclista;

import java.util.ArrayList;

public class CiclistaAcesso {
	private ArrayList<Ciclista> ciclistas = new ArrayList<Ciclista>();
	private static CiclistaAcesso ciclistaAcesso = null;

	CiclistaAcesso() {
		this.startArrayCiclistas();
	}

	public static CiclistaAcesso instanciaCiclista() {
		if (ciclistaAcesso == null) {
			ciclistaAcesso = new CiclistaAcesso();
		}
		return ciclistaAcesso;
	}

	public static void resetInstanciaCiclista() {
		ciclistaAcesso = null;
	}

	private void startArrayCiclistas() {
		this.ciclistas.add(new Ciclista("1", "pipoca@hotmail.com", "Pipoca Porto", "Pipoca_safadaum", true,
				new Documento("CPF", 8897654, "popcorn.png"),
				new CartaoBancario(8888987, "PIPOCA PORTO", "03/25", 445)));

		this.ciclistas.add(new Ciclista("2", "betobarbosa@gmail.com", "Betao", "Adocica_123", true,
				new Documento("CPF", 2345345, "foto.png"), new CartaoBancario(9908765, "Beto Barbosa", "09/23", 666)));

		this.ciclistas.add(new Ciclista("3", "marioquemario@gmail.com", "Mario", "atrasdoarmario", true));
	}

	public Ciclista getCiclistaById(String id) {
		for (Ciclista ciclista : this.ciclistas) {
			if (ciclista.getId().equals(id)) {
				return ciclista;
			}
		}
		return null;
	}

	public Ciclista getCiclistaByName(String nome) {
		for (Ciclista ciclista : this.ciclistas) {
			if (ciclista.getNome().equals(nome)) {
				return ciclista;
			}
		}
		return null;
	}

	public Ciclista getCiclistaByEmail(String email) {
		for (int i = 0; i < this.ciclistas.size(); i++) {
			if (this.ciclistas.get(i).getEmail().equals(email)) {
				return this.ciclistas.get(i);
			}
		}
		return null;
	}

	public void addCiclista(Ciclista c) {
		this.ciclistas.add(c);
	}

	public void deleteCiclista(String id) {
		Ciclista c = this.getCiclistaById(id);
		this.ciclistas.remove(c);
	}

	public ArrayList<Ciclista> getAllCiclistas() {
		return this.ciclistas;
	}

	public CartaoBancario getCartaoByUsuario(String prop) {
		System.out.println(prop);
		for (Ciclista ciclista : this.ciclistas) {
			CartaoBancario cartao = ciclista.getCartao();
			System.out.println(cartao.getUsuario());
			System.out.println(cartao.getUsuario().equals(prop));
			if (cartao.getUsuario().equals(prop)) {
				return cartao;
			}
		}
		return null;
	}

	public CartaoBancario getCartaoByNumero(int num) {
		for (Ciclista ciclista : this.ciclistas) {
			CartaoBancario cartao = ciclista.getCartao();
			if (cartao.getNumero() == num) {
				return cartao;
			}
		}
		return null;
	}

}
