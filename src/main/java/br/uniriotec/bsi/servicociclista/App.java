package br.uniriotec.bsi.servicociclista;

import static io.javalin.apibuilder.ApiBuilder.*;

import io.javalin.Javalin;

public class App {

	public static void main(String[] args) {
		Javalin.create(config -> {
			config.defaultContentType = "application/json";
		}).routes(() -> {
			//path("", () -> {
				//get(ctx -> ctx.result("Teste"));
			//});

			path("/ciclista", () -> {
				get(CiclistaControle::getCiclista);
				post(CiclistaControle::addCiclista);
				delete(CiclistaControle::deleteCiclista);
			});

			path("/ciclistas", () -> {
				get(CiclistaControle::getAllCiclistas);
			});

			path("/cartaobancario", () -> {
			});

			path("/email", () -> {
				get(CiclistaControle::getEmailCiclista);
			});

			path("/alugaBicicleta", () -> {
				post(CiclistaControle::alugaBicicleta);
			});
		}).start(getHerokuAssignedPort())
		  .get("/", ctx -> ctx.result("Hello Heroku"));
	}

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7070;
	}

}
