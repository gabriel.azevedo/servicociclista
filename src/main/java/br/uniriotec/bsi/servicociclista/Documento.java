package br.uniriotec.bsi.servicociclista;

public class Documento {
	private String tipo;
	private int numero;
	private String picture;
	
	public Documento() {
		//construtor
	}

	Documento(String tipo, int numero, String picture) {
		this.tipo = tipo;
		this.numero = numero;
		this.picture = picture;
	}

	public String getTipo() {
		return tipo;
	}

	public int getNumero() {
		return numero;
	}

	public String getPicture() {
		return picture;
	}
}
