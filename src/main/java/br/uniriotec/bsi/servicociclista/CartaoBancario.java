package br.uniriotec.bsi.servicociclista;

public class CartaoBancario {
	

	private String usuario;
	private int numero;
	private String validade;
	private int codigoSeg;
	
	public CartaoBancario() {
		 //construtor
	}
	
	CartaoBancario(int numero, String usuario, String validade, int codigoSeg) {
		this.numero = numero;
		this.usuario = usuario;
		this.validade = validade;
		this.codigoSeg = codigoSeg;
	}

	public int getNumero() {
		return numero;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getValidade() {
		return validade;
	}

	public int getCodigoSeg() {
		return codigoSeg;
	}

	
	

}
