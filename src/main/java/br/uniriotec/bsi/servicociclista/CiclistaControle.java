package br.uniriotec.bsi.servicociclista;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.util.ArrayList;

import br.uniriotec.bsi.servicociclista.BicicletaMock.BicicletaStatus;
import br.uniriotec.bsi.servicociclista.TrancaMock.TrancaStatus;
import io.javalin.http.Context;



public class CiclistaControle {
	
	
	public static void getCiclista(Context ctx) {
		CiclistaAcesso cAcess = CiclistaAcesso.instanciaCiclista();
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String email = ctx.queryParam("email");
		Ciclista c = null;
		
		if(id != null) {
			c = cAcess.getCiclistaById(id);
		}else if(nome != null) {
			c = cAcess.getCiclistaByName(nome);
		}else if(email != null) {
			c = cAcess.getCiclistaByEmail(email);
		}
		
		ctx.json(c);
	}
	
	public static void getAllCiclistas(Context ctx) {
		CiclistaAcesso cAcess = CiclistaAcesso.instanciaCiclista();
		ArrayList<Ciclista> ciclistas = cAcess.getAllCiclistas();
		ctx.status(200);
		ctx.json(ciclistas);
	}
	
	public static void addCiclista(Context ctx) {
		CiclistaAcesso cAcess = CiclistaAcesso.instanciaCiclista();
		Ciclista ciclista = ctx.bodyAsClass(Ciclista.class);
		cAcess.addCiclista(ciclista);
		ctx.status(201);
	}
	
	public static void deleteCiclista(Context ctx) {
		CiclistaAcesso cAcess = CiclistaAcesso.instanciaCiclista();
		String id = ctx.queryParam("id");
		if (id == null) {
			ctx.status(500);
		} else {
			id = String.valueOf(id);
			cAcess.deleteCiclista(id);			
			ctx.status(200);
		}
		
	}
	
	public static void getEmailCiclista(Context ctx) {
		CiclistaAcesso cAcess = CiclistaAcesso.instanciaCiclista();
		Ciclista c = null;
		String id = ctx.queryParam("id");
		String nome = ctx.queryParam("nome");
		String email = ctx.queryParam("email");
		
		if(id != null) {
			c = cAcess.getCiclistaById(id);
		}else if(nome != null) {
			c = cAcess.getCiclistaByName(nome);
		}else if(email != null) {
			c = cAcess.getCiclistaByEmail(email);
		}
		email = c.getEmail();
		ctx.json(email);
	}
	
	
	

	
	public static void alugaBicicleta(Context ctx) {
		String idCiclista = ctx.queryParam("Ciclista");
		String idTotem = ctx.queryParam("Totem");
		String numeroTranca = ctx.queryParam("Tranca");
		String idBicicleta = ctx.queryParam("Bicicleta");
		String idTranca = ctx.queryParam("idTranca");
		String isTest = ctx.queryParam("Teste");
		CiclistaAcesso cDao = CiclistaAcesso.instanciaCiclista();
		
		
		if(idCiclista == null || idTotem == null || numeroTranca == null || idBicicleta == null || idTranca == null) {
			ctx.status(400);
		}
		
		
		if(isTest != null && isTest.toLowerCase().equals("true")) {
			BicicletaMock.criaInstancias();
			TrancaMock.criaInstancias();
			
			BicicletaMock bicicleta = BicicletaMock.getBicicletaById(idBicicleta);
			TrancaMock tranca = TrancaMock.getTrancaById(idTranca);
			
			if(bicicleta == null || tranca == null) {
				ctx.status(400);				
			}
			
			if(bicicleta.getStatus() != BicicletaStatus.DISPONIVEL)
				ctx.status(400);
			
			if(tranca.getStatus() != TrancaStatus.EMUSO)
				ctx.status(400);
			
			bicicleta.setStatus(BicicletaStatus.EMUSO);
			tranca.setStatus(TrancaStatus.DISPONIVEL);
					
		}else {
			HttpResponse abreTrancaResponse = Unirest
					.post("https://javalin-heroku-equipamento.herokuapp.com"
						+ "/liberaTranca"
						+ "?idTotem="+ idTotem 
						+ "&numeroTranca="+ numeroTranca
						+ "&idTranca="+ idTranca
						+ "&idBicicleta"+ idBicicleta).asString();
				
				if(abreTrancaResponse.getStatus() == 400){
					ctx.status(400);
				}
				
				HttpResponse postStatusBicicletaResponse = Unirest
						.post("https://bicicletario.herokuapp.com/statusBicicleta" 
								+ "?emUso=true").asString();
				
				if(postStatusBicicletaResponse.getStatus() == 400){
					ctx.status(400);
				}
		}
		

		Ciclista c = cDao.getCiclistaById(idCiclista);
		
		if(c.biciletaEstaEmUso())
			ctx.status(400);
		
		c.setBiciletaEstaEmUso(true);
		
		ctx.status(200);
	}
	
	public static void devolveBicicleta(Context ctx){
		String idBicicleta = ctx.queryParam("Bicicleta");
		String idTranca = ctx.queryParam("Tranca");
		String idCiclista = ctx.queryParam("Ciclista");
		String isTest = ctx.queryParam("Teste");
		CiclistaAcesso cDao = CiclistaAcesso.instanciaCiclista();
		
		if (idBicicleta == null || idTranca == null) {
			ctx.status(400);
		}
		
		if (isTest != null && isTest.toLowerCase().equals("true")){			
			BicicletaMock.criaInstancias();
			TrancaMock.criaInstancias();
			
			BicicletaMock bicicleta = BicicletaMock.getBicicletaById(idBicicleta);
			TrancaMock tranca = TrancaMock.getTrancaById(idTranca);
			
			if(bicicleta == null || tranca == null)
				ctx.status(400);
	
			bicicleta.setStatus(BicicletaStatus.DISPONIVEL);
			tranca.setStatus(TrancaStatus.EMUSO);			
		} else{
			
			HttpResponse postStatusBicicletaResponse = Unirest
					.post("https://bicicletario.herokuapp.com/statusBicicleta" 
							+ "?emUso=false").asString();
			
			HttpResponse abreTrancaResponse = Unirest
					.post("https://javalin-heroku-equipamento.herokuapp.com"
						+ "/statusTranca"
						+ "?ocupada=true").asString();
			
		}
		
		Ciclista c = cDao.getCiclistaById(idCiclista);
		
		if(!c.biciletaEstaEmUso())
			ctx.status(400);
		
		c.setBiciletaEstaEmUso(false);
		
		ctx.status(200);			
	}

}
