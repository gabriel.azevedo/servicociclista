package br.uniriotec.bsi.servicociclista;

public class Ciclista {
	private String id = "";
	private String email = "";
	private String nome = "";
	private String senha = "";
	private boolean ehBrasileiro = true;
	private boolean bicicletaEmUso = false;
	private Documento documento;
	private CartaoBancario cartaoBancario;
	
	Ciclista() {
		this.id = "999999999999999";
		this.email = "xx@xx";
		this.nome = "XX";
		this.senha = "XXXX";
		this.ehBrasileiro = false;
		this.bicicletaEmUso = false;
	}
	
	Ciclista(String email, String nome, String senha, boolean ehBrasileiro) {
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.ehBrasileiro = ehBrasileiro;
	}

	Ciclista(String id, String email, String nome, String senha, boolean ehBrasileiro) {
		this.id = id;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.ehBrasileiro = ehBrasileiro;
	}
	
	Ciclista(String id, String email, String nome, 
			String senha, boolean indicadorBr, 
			Documento documento, CartaoBancario cartaoBancario) {
		this.id = id;
		this.email = email;
		this.nome = nome;
		this.senha = senha;
		this.ehBrasileiro = indicadorBr;
		this.documento = documento;
		this.cartaoBancario = cartaoBancario;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean ehBras() {
		return ehBrasileiro;
	}

	public void setEhBras(boolean ehBrasileiro) {
		this.ehBrasileiro = ehBrasileiro;
	}

	public boolean biciletaEstaEmUso() {
		return bicicletaEmUso;
	}

	public void setBiciletaEstaEmUso(boolean bicicletaEmUso) {
		this.bicicletaEmUso = bicicletaEmUso;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public CartaoBancario getCartao() {
		return cartaoBancario;
	}

	public void setCartao(CartaoBancario cartao) {
		this.cartaoBancario = cartao;
	}

}
