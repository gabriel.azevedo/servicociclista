package br.uniriotec.bsi.servicociclista;

import java.util.ArrayList;



public class TrancaMock {
	private String id;
	private int numero;
	private TrancaStatus status;
	public static ArrayList<TrancaMock> trancas;
	
	
	public TrancaMock(String id, int numero, TrancaStatus status){
		this.id = id;
		this.numero = numero;
		this.status = status;
	}
	
	public static TrancaMock getTrancaById(String id) {
		for(int i=0; i < trancas.size(); i++) {
			if (trancas.get(i).id == id){
				return trancas.get(i);
			}
		}
		return null;
	}
	
	public static void insertTranca(TrancaMock tranca){
		trancas.add(tranca);
	}
	
	public static void criaInstancias(){
		TrancaMock tranca1 = 
				new TrancaMock("01", 273273, TrancaStatus.EMUSO);
		TrancaMock tranca2 = 
				new TrancaMock("02", 273273, TrancaStatus.DISPONIVEL);
		TrancaMock tranca3 = 
				new TrancaMock("03", 273273, TrancaStatus.EMUSO);
		
		TrancaMock.insertTranca(tranca1);
		TrancaMock.insertTranca(tranca2);
		TrancaMock.insertTranca(tranca3);
	}
	
	public TrancaStatus getStatus(){
		return this.status;
	}
	
	public void setStatus(TrancaStatus status) {
		this.status = status;
	}
	
	
	public enum TrancaStatus{
		NOVA,
		EMUSO,
		DISPONIVEL,
		REPAROSOLICITADO,
		EMREPARO,
		APOSENTADA
	}
}
