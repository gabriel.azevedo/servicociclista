package br.uniriotec.bsi.servicociclista;


import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CiclistaAcessoTest {
	CiclistaAcesso cAcess;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		cAcess = CiclistaAcesso.instanciaCiclista();
	}

	@AfterEach
	void tearDown() throws Exception {
		CiclistaAcesso.resetInstanciaCiclista();
	}

	@Test
	void testeInstancia() {
		assertNotNull(cAcess);
	}

	@Test
	void getAllCiclistasTest() {
		ArrayList<Ciclista> ciclistas = cAcess.getAllCiclistas();
		assertEquals(3, ciclistas.size());
		assertEquals("Betao", ciclistas.get(1).getNome());
	}

	@Test
	void addCiclistaTest() {
		Ciclista c = new Ciclista("4", "cassiolindo@gmail.com", "Cassio", "Lindo1234", true);
		cAcess.addCiclista(c);
		ArrayList<Ciclista> ciclistas = cAcess.getAllCiclistas();
		assertEquals("Cassio", ciclistas.get(ciclistas.size() - 1).getNome());
	}

	@Test
	void getCiclistaByIdTest() {
		Ciclista c = cAcess.getCiclistaById("3");
		assertEquals("Mario", c.getNome());
	}

	@Test
	void getCiclistaByNameTest() {
		Ciclista c = cAcess.getCiclistaByName("Pipoca Porto");
		assertEquals("Pipoca Porto", c.getNome());
	}

	@Test
	void getCiclistaByEmailTest() {
		Ciclista c = cAcess.getCiclistaByEmail("marioquemario@gmail.com");
		assertEquals("marioquemario@gmail.com", c.getEmail());
	}

	@Test
	void deleteCiclistaTest() {
		cAcess.deleteCiclista("2");
		Ciclista c = cAcess.getCiclistaById("2");
		assertNull(c);
	}
	
	@Test
	void deleteCiclistaTest2() {
		cAcess.deleteCiclista("3");
		Ciclista c = cAcess.getCiclistaById("2");
		assertNotNull(c);
	}

	@Test
	void getCartaoByUsuarioTest() {
		CartaoBancario cartao = cAcess.getCartaoByUsuario("Beto Barbosa");
		assertEquals("Beto Barbosa", cartao.getUsuario());
	}

}
