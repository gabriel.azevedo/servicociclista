package br.uniriotec.bsi.servicociclista;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import io.javalin.http.Context;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IntegracaoTeste {

	CiclistaAcesso ca;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		ca = CiclistaAcesso.instanciaCiclista();
	}

	@AfterEach
	void tearDown() throws Exception {
		CiclistaAcesso.resetInstanciaCiclista();
	}
	
	
	
	@Test
	public void getCiclistasTest() {
		HttpResponse response = Unirest.get("http://localhost:7781/ciclistas").asString();
		assertThat(response.getStatus()).isEqualTo(200);	
	}

	@Test 
	public void getCiclistaTest() {
		
		String ciclistaJson1 = JavalinJson.toJson(ca.getCiclistaById("1"));
		HttpResponse response1 = Unirest.get("http://localhost:7781/ciclista?id=1").asString();
		assertThat(response1.getBody()).isEqualTo(ciclistaJson1);
		
		
		String ciclistaJson2 = JavalinJson.toJson(ca.getCiclistaByName("Mario"));
		HttpResponse response2 = Unirest.get("http://localhost:7781/ciclista?nome=Mario").asString();
		assertThat(response2.getBody()).isEqualTo(ciclistaJson2);
		
		
		
		String ciclistaJson3 = JavalinJson.toJson(ca.getCiclistaByEmail("marioquemario@gmail.com"));
		HttpResponse response3 = Unirest.get("http://localhost:7781/ciclista?email=marioquemario@gmail.com").asString();
		assertThat(response3.getBody()).isEqualTo(ciclistaJson3);
		
	}
	
	@Test 
	public void getCiclistaTestFail() {
		
		HttpResponse response1Error = Unirest.get("http://localhost:7781/ciclista?id=-1000").asString();
		assertThat(response1Error.getStatus()).isEqualTo(500);
		
		HttpResponse response2Error = Unirest.get("http://localhost:7781/ciclista?nome=wdbhdbhdbh").asString();
		assertThat(response2Error.getStatus()).isEqualTo(500);
		
		HttpResponse response3Error = Unirest.get("http://localhost:7781/ciclista?email=ndwdjdwjnd").asString();
		assertThat(response3Error.getStatus()).isEqualTo(500);
	}
	
	@Test
	public void postCiclistaTest() {
		
		Ciclista c = new Ciclista(
				"4",
				"hahaha@gmail.com",
				"Arthur",
				"1567", true
			);
		
		String ciclistaJson = JavalinJson.toJson(c);
		HttpResponse response = Unirest.post("http://localhost:7781/ciclista").body(ciclistaJson).asString();
		assertThat(response.getStatus()).isEqualTo(201);
	}
	
	@Test
	public void postCiclistaTestFail() {		
		Ciclista c = null;
		HttpResponse response = Unirest.post("http://localhost:7781/ciclista").body(c).asString();
		assertThat(response.getStatus()).isEqualTo(400);
	}
	
	@Test
	public void deleteCiclistaTest() {
		HttpResponse response = Unirest.delete("http://localhost:7781/ciclista?id=01").asString();
		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void deleteCiclistaTestFail() {
		HttpResponse response = Unirest.delete("http://localhost:7781/ciclista?id=").asString();
		assertThat(response.getStatus()).isEqualTo(200);
	}

	@Test
	public void getEmailTest() {
		String emailJson1 = JavalinJson.toJson(ca.getCiclistaById("1").getEmail());
		HttpResponse response1 = Unirest.get("http://localhost:7781/email?id=1").asString();
		assertThat(response1.getBody()).isEqualTo(emailJson1);
		
		String emailJson2 = JavalinJson.toJson(ca.getCiclistaByName("Mario").getEmail());
		HttpResponse response2 = Unirest.get("http://localhost:7781/email?nome=Mario").asString();
		assertThat(response2.getBody()).isEqualTo(emailJson2);
		
		String emailJson3 = JavalinJson.toJson(ca.getCiclistaByEmail("marioquemario@gmail.com").getEmail());
		HttpResponse response3 = Unirest.get("http://localhost:7781/email?email=marioquemario@gmail.com").asString();
		
	}
	
	@Test
	public void getEmailTestFail() {
		HttpResponse response1 = Unirest.get("http://localhost:7781/email?id=-102992").asString();
		assertThat(response1.getStatus()).isEqualTo(500);
		
		HttpResponse response2 = Unirest.get("http://localhost:7781/email?nome=dsdsds").asString();
		assertThat(response2.getStatus()).isEqualTo(500);
		
		HttpResponse response3 = Unirest.get("http://localhost:7781/email?email=sdfedwd").asString();
		assertThat(response3.getStatus()).isEqualTo(500);
	}

	
	@Test 
	public void getcartaobancarioTestFail() {
		HttpResponse response1 = Unirest.get("http://localhost:7781/cartaobancario?id=-1828").asString();
		assertThat(response1.getStatus()).isEqualTo(404);
		
		HttpResponse response2 = Unirest.get("http://localhost:7781/cartaobancario?usuario=dssfa").asString();
		assertThat(response2.getStatus()).isEqualTo(404);
		
		HttpResponse response3 = Unirest.get("http://localhost:7781/cartaobancario?numero=-027182").asString();
		assertThat(response3.getStatus()).isEqualTo(404);
	}
	

	@Test 
	public void alugaBicicletaTest() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=01"
						+ "&Totem=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=01"
						+ "&idTranca=01"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}
	
	@Test 
	public void alugaBicicletaTestFail() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=01"
						+ "&Totem=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=10"
						+ "&idTranca=01"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}
	
	@Test 
	public void devolveBicicletaTest() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "Ciclista=01"
						+ "&Tranca=-12343"
						+ "&Bicicleta=01"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}
	
	@Test 
	public void devolveBicicletaTestFail() {
		HttpResponse response = Unirest
				.post("http://localhost:7781/alugaBicicleta?"
						+ "idCiclista=01"
						+ "&Tranca=12343"
						+ "&Bicicleta=-1000"
						+ "&Teste=true").asString();
		
		assertThat(response.getStatus()).isEqualTo(500);
	}
	
}
